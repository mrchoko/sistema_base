<?php
require_once "conexion.php";
require_once "errores.php";
require_once "./modelos.php";

class Init extends Errores{

	public function crea_base_datos($nombre_base_datos = False){ //pruebas finalizadas
		$conexion = new Conexion();
		$link = $conexion->link;
		if(!$nombre_base_datos){
			$nombre_base_datos = $conexion->nombre_base_datos;
		}


		$existe_bd = $this->existe_bd($nombre_base_datos);

		if(!$existe_bd){
			$consulta = "CREATE DATABASE ".$nombre_base_datos;
			$link->query($consulta);
			if($link->error){
				$this->error(1,__LINE__,__FILE__);
			}
			else{
				$this->mensaje("Se creo la base de datos: ".$nombre_base_datos,__LINE__,__FILE__);
				$this->error(-1,__LINE__,__FILE__);
				return true;				
			}
		}
		else{
			$this->error(4,__LINE__,__FILE__);
		}
		if($this->numero_error){
			return false;
		}
	}


	public function crea_campo($campo=False){
		$campo_base_datos = $campo['nombre_campo'].' '.$campo['tipo_dato'];
		$campo_base_datos = $campo_base_datos.' '.$campo['autoincrement'];
		$campo_base_datos = $campo_base_datos.' '.$campo['nulo'];
		$campo_base_datos = $campo_base_datos.' '.$campo['primary'];
		return $campo_base_datos;
	}
	public function crea_estructura($nombre_base_datos=False, $estructura = False){ //prueba en proceso
		$conexion = new Conexion();
		$link = $conexion->link;
		if(!$estructura){
			$modelo_base = new Modelos();
			$estructura = $modelo_base->genera_estructura();
		}
		if(!$nombre_base_datos){
			$nombre_base_datos = $conexion->nombre_base_datos;
		}

		if(!is_array($estructura)){//error probado
			$this->error(5,__LINE__,__FILE__);
		}
		else{
			foreach ($estructura as $key => $tabla) { //error probado
				if(!is_array($tabla)){
					$this->error(6,__LINE__,__FILE__);
					break;
				}
				else{
					$campos = $tabla['campos'];

					if(!is_array($campos)){
						$this->error(7,__LINE__,__FILE__);
						break;
					}
					else{
						foreach ($campos as $key => $campo) {
							if(!is_array($campo)){ //error probado
								$this->error(8,__LINE__,__FILE__);
								break;
							}
							else{
								$contador_atributos = 0;
								$error = False;
								foreach ($campo as $key => $atributo) {
									$atributos_validos = array(
										'nombre_campo','tipo_dato','nulo','primary','autoincrement');
									$numero_atributos_validos=count($atributos_validos);
									if(!in_array($key, $atributos_validos)){ // error probado
										$this->error(9,__LINE__,__FILE__);
										$error = true;
										break;
									}
									else{
										$contador_atributos++;
									}
								}
								if(!$error){
									if($this->valida_atributo($campo)){
										$campo_id = $this->crea_campo($campo);
										$this->mensaje("Se creo el campo: ". $campo['nombre_campo'],__LINE__,__FILE__);
									}
									else{
										$this->error(22,__LINE__,__FILE__);
										break;
									}
									if($contador_atributos<$numero_atributos_validos){ //error probado
										$this->error(10,__LINE__,__FILE__);
										break;
									}
								}
							}
						}
					}

				}
			}
		}
		if($this->numero_error){
			return false;
		}
	}

	public function elimina_base_datos($nombre_base_datos=False){
		$conexion = new Conexion();
		$link = $conexion->link;
		if(!$nombre_base_datos){
			$nombre_base_datos = $conexion->nombre_base_datos;
		}

		$existe_bd = $this->existe_bd($nombre_base_datos);

		if($existe_bd){
			$consulta = "DROP DATABASE ".$nombre_base_datos;
			$link->query($consulta);
			if($link->error){
				$this->error(3,__LINE__,__FILE__);
			}
			else{
				$this->mensaje("Se eliminó la base de datos: ".$nombre_base_datos,__LINE__,__FILE__);
				$this->error(-1,__LINE__,__FILE__);
				return true;				
			}
		}
		else{
			$this->error(2,__LINE__,__FILE__);
		}
		if($this->numero_error){
			return false;
		}
	}

	public function existe_bd($nombre_base_datos=False){
		$conexion = new Conexion();
		$link = $conexion->link;
		if(!$nombre_base_datos){
			$nombre_base_datos = $conexion->nombre_base_datos;
		}
		$existe = $conexion->selecciona_base_datos($nombre_base_datos);
		$this->error(-1,__LINE__,__FILE__);
		if($existe){
			return True;
		}
		else{
			return False;
		}
	}

	public function valida_atributo($campo=False){
		if(!$this->valida_atributo_tipo_dato($campo)){
			$this->error(15,__LINE__,__FILE__);
		}
		elseif(!$this->valida_atributo_nulo($campo)){
			$this->error(14,__LINE__,__FILE__);
		}
		elseif(!$this->valida_atributo_primary($campo)){
			$this->error(13,__LINE__,__FILE__);
		}
		elseif(!$this->valida_atributo_autoincrement($campo)){
			$this->error(12,__LINE__,__FILE__);
		}
		else{
			return true;
		}
		if($this->numero_error){
			return false;
		}			

	}

	public function valida_atributo_autoincrement($campo=False){
		if($campo['nombre_campo']=='id'){
			if($campo['autoincrement'] == 'AUTO_INCREMENT'){
				return true;
			}
			else{
				$this->error(12,__LINE__,__FILE__);
			}			
		}
		else{
			if($campo['autoincrement']){
				$this->error(16,__LINE__,__FILE__);
			}
			else{
				return true;
			}
		}
		if($this->numero_error){
			return false;
		}		
	}



	public function valida_atributo_nulo($campo=False){
		$campos_sin_nulo = ['id', 'descripcion', 'status'];

		if(in_array($campo['nombre_campo'], $campos_sin_nulo)){
			if($campo['nulo']=='NOT NULL'){
				return true;
			}
			else{ 
				$this->error(18,__LINE__,__FILE__);
			}
		}
		else{
			if($campo['nulo']){
				$this->error(19,__LINE__,__FILE__);	
			}
			else{
				return true;
			}
		}
		if($this->numero_error){
			return false;
		}
	}



	public function valida_atributo_primary($campo=False){
		if($campo['nombre_campo']=='id'){
			if($campo['primary'] == 'PRIMARY KEY'){
				return true;
			}
			else{
				$this->error(13,__LINE__,__FILE__);
			}
		}
		else{
			if($campo['primary']){
				$this->error(17,__LINE__,__FILE__);
			}
			else{
				return true;
			}
		}
		if($this->numero_error){
			return false;
		}		
	}

	public function valida_atributo_tipo_dato($campo=False){
		$campos_int_11 = ['id'];
		if(in_array($campo['nombre_campo'], $campos_int_11)){	
			if(array_key_exists('tipo_dato', $campo)){
				if($campo['tipo_dato']=='int(11)'){
					return true;
				}
				else{
					$this->error(20,__LINE__,__FILE__);
				}
			}
			else{
				$this->error(23,__LINE__,__FILE__);
			}
		}
		else{
			if(array_key_exists('tipo_dato', $campo)){
				if($campo['tipo_dato']=='int(11)'){
					$this->error(21,__LINE__,__FILE__);
				}
				else{
					return true;
				}
			}
		}
		if($this->numero_error){
			return false;
		}				
	}

}

?>