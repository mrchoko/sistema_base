<?php

class Errores{

	public $numero_error;
	public $mensaje;
	public $linea;
	public $archivo;
	public $muestra_errores=0;

	public function error($numero_error, $linea,$archivo){
		if($numero_error == 1){
			$this->numero_error = 1;
			$this->mensaje="Error al seleccionar base de datos";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 2) {
			$this->numero_error = 2;
			$this->mensaje="Error no existe la base de datos";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 3) {
			$this->numero_error = 3;
			$this->mensaje="Error al eliminar la base de datos";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 4) {
			$this->numero_error = 3;
			$this->mensaje="Error al crear la base de datos por que ya existe";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 5) {
			$this->numero_error = 5;
			$this->mensaje="La estructura debe de ser un arreglo";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 6) {
			$this->numero_error = 6;
			$this->mensaje="La estructura de la tabla debe de ser un arreglo";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 7) {
			$this->numero_error = 7;
			$this->mensaje="La estructura de los campos debe de ser un arreglo";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 8) {
			$this->numero_error = 8;
			$this->mensaje="El campo debe de ser un arreglo";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 9) {
			$this->numero_error = 9;
			$this->mensaje="Nombre de atributo inválido";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 10) {
			$this->numero_error = 10;
			$this->mensaje="Faltan atributos";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}				
		elseif ($numero_error == 12) {
			$this->numero_error = 12;
			$this->mensaje="El campo id debe de ser autoincrement";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 13) {
			$this->numero_error = 13;
			$this->mensaje="El campo id debe de ser primary";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}						
		elseif ($numero_error == 14) {
			$this->numero_error = 14;
			$this->mensaje="El campo id debe de ser not null";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 15) {
			$this->numero_error = 15;
			$this->mensaje="El campo id debe de ser int(11)";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 16) {
			$this->numero_error = 16;
			$this->mensaje="El atributo autoincrement no puede ser True";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 17) {
			$this->numero_error = 17;
			$this->mensaje="El atributo primary no puede ser True";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}												
		elseif ($numero_error == 18) {
			$this->numero_error = 18;
			$this->mensaje="El atributo nulo tiene que se True";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}	
		elseif ($numero_error == 19) {
			$this->numero_error = 19;
			$this->mensaje="El atributo nulo tiene que se False";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 20) {
			$this->numero_error = 20;
			$this->mensaje="El atributo tipo dato tiene que se int(11)";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 21) {
			$this->numero_error = 21;
			$this->mensaje="El atributo tipo dato no puede ser que se int(11)";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 22) {
			$this->numero_error = 22;
			$this->mensaje="Error en los atributos";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 23) {
			$this->numero_error = 23;
			$this->mensaje="No existe el atributo tipo_dato";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 24) {
			$this->numero_error = 24;
			$this->mensaje="El registro debe de ser un arreglo";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 25) {
			$this->numero_error = 25;
			$this->mensaje="El campo debe ser varchar";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 26) {
			$this->numero_error = 26;
			$this->mensaje="El campo debe ser text";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 27) {
			$this->numero_error = 27;
			$this->mensaje="El campo debe ser tinyint";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 28) {
			$this->numero_error = 28;
			$this->mensaje="Error en el valor del registro";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 29) {
			$this->numero_error = 29;
			$this->mensaje="El array no debe estar vacio";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 30) {
			$this->numero_error = 30;
			$this->mensaje="El campo debe ser smallint";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 31) {
			$this->numero_error = 31;
			$this->mensaje="El campo debe ser timestamp";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 32) {
			$this->numero_error = 32;
			$this->mensaje="No existe la tabla en los modelos";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 33) {
			$this->numero_error = 33;
			$this->mensaje="No existe el campo en la tabla";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 34) {
			$this->numero_error = 34;
			$this->mensaje="No puede ser nulo el valor";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 35) {
			$this->numero_error = 35;
			$this->mensaje="Falta un campo obligatorio";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 36) {
			$this->numero_error = 36;
			$this->mensaje="Error al insertar registro";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 37) {
			$this->numero_error = 37;
			$this->mensaje="Valor nulo no permitido";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 38) {
			$this->numero_error = 38;
			$this->mensaje="Faltan campos obligatorios";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 39) {
			$this->numero_error = 39;
			$this->mensaje="Error en la obtencion del registro";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}
		elseif ($numero_error == 40) {
			$this->numero_error = 40;
			$this->mensaje="Error en eliminar  registro";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}																
		elseif ($numero_error == 41) {
			$this->numero_error = 41;
			$this->mensaje="Error en actualizacion de status";
			$this->linea = $linea;
			$this->archivo = $archivo;
		}	
		else{
			$this->numero_error = False;
			$this->mensaje=False;
			$this->linea = False;
			$this->archivo = False;
		}
		if($this->numero_error && $this->muestra_errores==1){
			echo "<br>!!Error:".$this->numero_error." Mensaje: ".$this->mensaje.' Linea: '.$this->linea; 
			echo " Archivo: ".$archivo."<br>";
		}

	}
	public function mensaje($mensaje,$linea,$archivo){
			echo "<br>Mensaje: ".$mensaje.' Linea: '.$linea; 
			echo " Archivo: ".$archivo."<br>";		
	}
}
?>