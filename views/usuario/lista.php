<?php
if (!empty($usuarios)) {
?>

<div id="usuarios_lista">
	<div class="container">
		<div class="row">
			<div class="page-header text-center"><h3>Lista</h3></div>
		</div>
	</div>
	<div class="container">
		<div class="row hiden-btn">
			<div class="input-group col-xs-6 col-xs-offset-6">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-1" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
		<div class="row hidden">
			<div class="input-group col-xs-12">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-2" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="table-responsive" id="lista_usuarios">
					<table class="table table-fixed">
    					<thead>
      						<tr>
      							<th class="col-xs-1">Id</th>
        						<th class="col-xs-2">Usuario</th>
        						<th class="col-xs-3">Email</th>
        						<th class="col-xs-2 hiden-btn">Grupo Descripcion</th>
        						<th class="col-xs-2 hidden">Grupo</th>
        						<th class="col-xs-4 text-center">Acciones</th>
      						</tr>
    					</thead>
    					<tbody class="registros">
						<?php foreach ($usuarios as $key => $usuario) { ?>
							<tr>
								<td class="col-xs-1"><?php echo $usuario['id']; ?></td>
								<td class="col-xs-2 ocultar-texto"><?php echo $usuario['user']; ?></td>
								<td class="col-xs-3 ocultar-texto"><?php echo $usuario['email']; ?></td>
								<td class="col-xs-2 ocultar-texto"><?php echo $usuario['descripcion']; ?></td>
								<td class="col-xs-2 hiden-btn">
									<div class="btn-group">
										<a href="index.php?seccion=usuario&accion=elimina&usuario_id=<?php echo $usuario['id']; ?>">
  											<button type="button" class="btn btn-danger">
  												Elimina
  											</button>
  										</a>
  									</div>
  								</td>
  								<td class="col-xs-2 hiden-btn">
  									<div class="btn-group">
  										<a href="index.php?seccion=usuario&accion=modifica&usuario_id=<?php echo $usuario['id']; ?>">
  										<button type="button" class="btn btn-primary">
  											Modifica
  										</button>
  										</a>
									</div>
								</td>


								<td class="col-xs-1 hidden"></td>
								<td class="col-xs-3 text-center hidden">

								    <div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									    	<span>Acciones</span>
									    	<span></span>
									    	<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">
											<li>
												<a href="index.php?seccion=usuario&accion=elimina&usuario_id=<?php echo $usuario['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline">
		  												Elimina
		  											</button>
		  										</a>
											</li>
											<li>
												<a href="index.php?seccion=usuario&accion=modifica&usuario_id=<?php echo $usuario['id']; ?>">
		  										<button type="button" class="btn btn-secondary btn-outline">
		  											Modifica
		  										</button>
		  										</a>
											</li>
										</ul>
									</div>

								</td>



							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}
?>