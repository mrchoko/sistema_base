<div id="seccion_alta">
	<div class="page-header text-center"><h4>Modifica</h4></div>
	<form 
		id="form-seccion-alta" name="form-seccion-alta" 
		method="post" 
		action="./index.php?seccion=usuario&accion=modifica_bd&usuario_id=<?php echo $usuario_id; ?>">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="usuario" placeholder="Ingresa Usuario" 
					required title="Ingrese un usuario"
					value='<?php echo $usuario[0]['user']; ?>'>
				<div class="row">&nbsp;</div>
			</div>
			<div class="col-md-6">
				<input 
					type="password" class="form-control" 
					name="contrasena" placeholder="Ingresa Contraseña" 
					required title="Ingrese una contraseña"
					value='<?php echo $usuario[0]['password']; ?>'>
				<div class="row">&nbsp;</div>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="email" class="form-control" 
					name="correo" placeholder="Ingresa Correo" 
					required title="Ingrese un correo"
					value='<?php echo $usuario[0]['email']; ?>'>
				<div class="row">&nbsp;</div>
			</div>
			<div class="col-md-6">
				<select name="grupo_id" class="selectpicker" data-live-search="true" 
				title="Seleccione un grupo" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        <?php
			        	foreach ($valores as $key => $grupo) {
			        		echo '<option value="'.$grupo[id].'"';
			        			if ($usuario[0]['grupo_id'] == $grupo['id']) {
			        				echo 'selected';	
			        		}
			        		echo '>'.$grupo['descripcion'].'</option>';
			        	}
			        ?>
			    </select>
			</div>

		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary" >Enviar</button>
			</div>
		</div>

	</form>
</div>