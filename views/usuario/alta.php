<div id="seccion_alta">
	<div class="page-header text-center"><h4>Alta</h4></div>
	<form 
		id="form-seccion-alta" name="form-seccion-alta" 
		method="post" 
		action="./index.php?seccion=usuario&accion=alta_bd">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="user" placeholder="Ingresa Usuario" 
					required title="Ingrese un usuario">
				<div class="row">&nbsp;</div>
			</div>
			<div class="col-md-6">
				<input 
					type="password" class="form-control" 
					name="password" placeholder="Ingresa Contraseña" 
					required title="Ingrese una contraseña">
				<div class="row">&nbsp;</div>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="email" class="form-control" 
					name="email" placeholder="Ingresa Correo" 
					required title="Ingrese un correo">
				<div class="row">&nbsp;</div>
			</div>
			<div class="col-md-6">
				<select name="grupo_id" class="selectpicker" data-live-search="true" 
				title="Seleccione un grupo" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
					<?php
					   	foreach ($valores as $key => $grupo) {
					   		echo '<option value="'.$grupo[id].'">'.$grupo[descripcion].'</option>';
					   	}
					?>
				</select>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary" >Enviar</button>
			</div>
		</div>
	</form>
</div>