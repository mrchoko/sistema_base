<div id="seccion_alta">
	<div class="page-header text-center"><h4>Alta</h4></div>
	<form 
		id="form-seccion-alta" name="form-seccion-alta" 
		method="post" 
		action="./index.php?seccion=accion_grupo&accion=alta_db">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-6">
				<select name="accion_id" class="selectpicker" data-live-search="true" 
				title="Seleccione una accion" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        <?php
			        	foreach ($valores1 as $key => $accion) {
			        		echo '<option value="'.$accion[id].'">'.$accion[descripcion].'</option>';
			        	}
			        ?>
			    </select>
			</div>
			<div class="col-md-6">
				<select name="grupo_id" class="selectpicker" data-live-search="true" 
				title="Seleccione un grupo" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        <?php
			        	foreach ($valores2 as $key => $grupo) {
			        		echo '<option value="'.$grupo[id].'">'.$grupo[descripcion].'</option>';
			        	}
			        ?>
			    </select>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary" >Enviar</button>
			</div>
		</div>

	</form>
</div>