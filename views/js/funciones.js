$(function(){ 
    var navMain = $("#nav-main");

    navMain.on("click", "p", null, function () {
	    navMain.collapse('hide');
	    $('.collapse').collapse('hide');
	    //$('.form-group input[type="text"]').val('');
    });
});

function toTop() {
	window.scrollTo(0, 0)
}

$(document).ready(function () {
    $('#busqueda-1').keyup(function () {
        document.getElementById("busqueda-2").value = ""; 
        var rex = new RegExp($(this).val(), 'i');
        $('.registros tr').hide();
        $('.registros tr').filter(function () {
            return rex.test($(this).text());
        }).show();

        });
    $('#busqueda-2').keyup(function () {
        document.getElementById("busqueda-1").value = ""; 
        var rex = new RegExp($(this).val(), 'i');
        $('.registros tr').hide();
        $('.registros tr').filter(function () {
            return rex.test($(this).text());
        }).show();

        });

    $('.foto').click(function() {
        var src =$(this).attr('src');

        $('.zoom_img').attr('src', src);
    });

});

function resetForm($form) {
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox')
    .removeAttr('checked').removeAttr('selected');
}





