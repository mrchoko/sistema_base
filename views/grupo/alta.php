<div id="grupo_alta">
	<div class="page-header text-center"><h4>Alta</h4></div>
	<form 
		id="form-grupo-alta" name="form-grupo-alta" 
		method="post" 
		action="./index.php?seccion=grupo&accion=alta_bd">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-10">
				<input 
					type="text" class="form-control" 
					name="descripcion" placeholder="Ingresa Descripción" 
					required title="Ingrese una descripción">
				<div class="row">&nbsp;</div>
			</div>
			<div class="col-md-2 text-right">
		    	<label class="switch1">
					<input type="checkbox" name="status" >
					<div class="slider1"></div>
				</label>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<textarea class="form-control noresize" name="observaciones" rows="3" placeholder="Ingresa Observaciones" title="Ingrese una observacion"></textarea>
			</div>
		</div>
		<div class="form-group text-center">
			<button type="submit" class="btn btn-secondary" >Enviar</button>
		</div>
	</form>

</div>