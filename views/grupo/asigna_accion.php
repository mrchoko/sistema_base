<script type="text/javascript">
    $(document).ready(function() {

        $(":checkbox").click(function(){
            var accion = $(this).attr('name');
            var grupo = $( "#grupo_id" ).val();
            var accion_grupo = $(this).attr('value');

            //alert(accion_grupo);
            if ($(this).prop("checked") == true){

                $.ajax({ //./index.php?seccion=accion_grupo&accion=alta_db
                        url: "./index.php?seccion=accion_grupo&accion=alta_db",
                        type: "POST", //send it through get method
                        data: {
                            grupo_id: grupo,
                            accion_id: accion
                        },
                        success: function(data) {
                            //Do Something
                            //alert("insertado correctamente");
                        },
                        error: function(xhr, status) {
                            //Do Something to handle error
                            //alert("no insertado correctamente");
                        }
                    });
                location.reload();
                $(this).attr("checked") = "checked";

            } else {
                
                $.ajax({ //index.php?seccion=accion_grupo&accion=elimina&accion_grupo_id; 
                        url: "./index.php?seccion=accion_grupo&accion=elimina&accion_grupo_id="+accion_grupo,
                        type: "POST", //send it through get method
                        data: {
                            accion_grupo_id: accion_grupo
                        },
                        success: function(data) {
                            //Do Something
                            //alert("eliminado correctamente");
                        },
                        error: function(xhr, status) {
                            //Do Something to handle error
                            //alert("no eliminado correctamente");
                        }
                    });
                location.reload();
                $(this).attr("checked") = "";
            }

            return false;
        });


    });
</script>

<div id="grupo_alta">
    <div class="page-header text-center"><h4>Asigna Accion</h4></div>
    <form 
        id="form-asigna-accion" name="form-asigna-accion" 
        method="post" 
        action="./index.php?seccion=grupo&accion=asigna_accion&grupo_id=<?php echo $grupo_id; ?>">
        <div class="text-center">
            <h3>
            <?php echo $grupo[0]['descripcion']; ?>
            </h3>
        </div>
        
        <div class='row well'>
            <div class='col-md-12 scroll'>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">

                        <?php
                        foreach ($secciones as $key => $seccion) {
                            echo "<div class='panel-heading'>";
                            echo "<h4 class='panel-title'>";
                            echo "<a data-toggle='collapse' data-parent='#accordion' href='#".$seccion['id']."' >".$seccion['descripcion']."</a>";
                            echo "</div>";
                            echo "<div id='".$seccion['id']."' class='panel-collapse collapse in'>";
                            echo "<div class='panel-body'>";
                            echo "<ul class='list-group' id='lista-seccion-accion'>";
                                
                            
                            foreach ($acciones as $key => $accion) {
                                if($seccion['descripcion'] == $accion['descripcion_seccion']){

                                    echo "<li class='list-group-item'>".$accion['descripcion'];
                                    echo "<div class='material-switch pull-right'>";
                                    echo "<input id='".$accion['descripcion']."' name='".$accion['id']."' type='checkbox' ";
                                    if(!empty($acciones_grupos)){
                                    $si_check = false;
                                    foreach ($acciones_grupos as $key => $valores) {
                                        if($grupo[0]['descripcion'] == $valores['descripcion_grupo'] &&
                                            $accion['descripcion'] == $valores['descripcion'] && 
                                            $seccion['descripcion'] == $accion['descripcion_seccion']){
                                            echo "value =".$valores['id']." name='".$accion['id']."' checked />";
                                            $si_check = true;
                                        }
                                    }
                                    if(!$si_check){echo "value='' />"; }
                                    }
                                    else{
                                        echo "value='' />";   
                                    }
                                    echo "<label for='".$accion['descripcion']."' class='label-primary'></label>";
                                    echo "</div>";
                                    echo "</li>";
                                }   

                            }



                            echo "</ul>";
                            echo "</div>";
                            echo "</div>";
                        }
                        ?>



                    </div>
                </div>
            </div> 
        </div>



        <div class="form-group text-center">
            <input type='hidden' id='grupo_id' value='<?php echo $grupo[0]['id']; ?>'>
        </div>
    </form>
</div>