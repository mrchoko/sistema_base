<?php
if (!empty($grupos)) {
?>

<div id="grupo_lista">
	<div class="container">
		<div class="row">
			<div class="page-header text-center"><h3>Lista</h3></div>
		</div>
	</div>
	<div class="container">
		<div class="row hiden-btn">
			<div class="input-group col-xs-6 col-xs-offset-6">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-1" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
		<div class="row hidden">
			<div class="input-group col-xs-12">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-2" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default" id="lista_grupo">
					<table class="table table-fixed">
    					<thead>
      						<tr>
      							<th class="col-xs-1">Id</th>
        						<th class="col-xs-4">Descripcion</th>
        						<th class="col-xs-1">Estatus</th>
        						<th class="col-xs-6 text-center">Acciones</th>
      						</tr>
    					</thead>
    					<tbody class="registros">
						<?php foreach ($grupos as $key => $grupo) { ?>
							<tr>
								<td class="col-xs-1"><?php echo $grupo['id']; ?></td>
								<td class="col-xs-4 ocultar-texto"><?php echo $grupo['descripcion']; ?></td>
								<td class="col-xs-1">
									<div>
										<?php if($grupo['status']==1){ echo 'Activo';}
												else{ echo 'Inactivo'; } ?>
									</div>
								</td>
								<td class="col-xs-1 text-center hiden-btn">
									<div class="btn-group">
										<a href="index.php?seccion=grupo&accion=elimina&grupo_id=<?php echo $grupo['id']; ?>">
  											<button type="button" class="btn btn-danger" >
  												Elimina
  											</button>
  										</a>
  									</div>
  								</td>
  								<td class="col-xs-2 text-center hiden-btn">
									<div class="btn-group">
  										<?php if($grupo['status']==1){ ?>
  										<a href="index.php?seccion=grupo&accion=desactiva&grupo_id=<?php echo $grupo['id']; ?>">
  											<button type="button" class="btn btn-primary" >
  												Desactiva
  											</button>
  										</a>
  										<?php }
  										else{ ?>

  										<a href="index.php?seccion=grupo&accion=activa&grupo_id=<?php echo $grupo['id']; ?>">
  											<button type="button" class="btn btn-primary">
  												Activa
  											</button>
  										</a>
  										<?php
  										}
  										?>
  									</div>
  								</td>
  								<td class="col-xs-1 text-center hiden-btn">
									<div class="btn-group">
  										<a href="index.php?seccion=grupo&accion=modifica&grupo_id=<?php echo $grupo['id']; ?>">
  										<button type="button" class="btn btn-primary">
  											Modifica
  										</button>
  										</a>
									</div>
								</td>
								<td class="col-xs-2 text-center hiden-btn">
									<div class="btn-group">
  										<a href="index.php?seccion=grupo&accion=asigna_accion&grupo_id=<?php echo $grupo['id']; ?>">
  										<button type="button" class="btn btn-primary">
  											Accion
  										</button>
  										</a>
									</div>
								</td>

								<td class="col-xs-1 hidden"></td>
								<td class="col-xs-5 text-center hidden">

								    <div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									    	<span>Acciones</span>
									    	<span></span>
									    	<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">
											<li>
												<a href="index.php?seccion=grupo&accion=elimina&grupo_id=<?php echo $grupo['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline" >
		  												Elimina
		  											</button>
		  										</a>
											</li>
											<li>
												<?php if($grupo['status']==1){ ?>
		  										<a href="index.php?seccion=grupo&accion=desactiva&grupo_id=<?php echo $grupo['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline" >
		  												Desactiva
		  											</button>
		  										</a>
		  										<?php }
		  										else{ ?>

		  										<a href="index.php?seccion=grupo&accion=activa&grupo_id=<?php echo $grupo['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline">
		  												Activa
		  											</button>
		  										</a>
		  										<?php
		  										}
		  										?>
											</li>
											<li>
												<a href="index.php?seccion=grupo&accion=modifica&grupo_id=<?php echo $grupo['id']; ?>">
		  										<button type="button" class="btn btn-secondary btn-outline">
		  											Modifica
		  										</button>
		  										</a>
											</li>
									    	<li>
									    		<a href="index.php?seccion=grupo&accion=asigna_accion&grupo_id=<?php echo $grupo['id']; ?>">
		  										<button type="button" class="btn btn-secondary btn-outline">
		  											Accion
		  										</button>
		  										</a>
											</li>
										</ul>
									</div>

								</td>


							</tr>
						<?php } ?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>
<?php
}
?>
	