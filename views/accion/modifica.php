<div id="seccion_alta">
	<div class="page-header text-center"><h4>Modifica</h4></div>
	<form 
		id="form-seccion-alta" name="form-seccion-alta" 
		method="post" 
		action="./index.php?seccion=accion&accion=modifica_bd&accion_id=<?php echo $accion_id; ?>">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="descripcion" placeholder="Ingresa Descripcion" 
					required title="Ingrese una descripcion"
					value='<?php echo $accion1[0]['descripcion']; ?>'>
				<div class="row">&nbsp;</div>
			</div>
		<div class="col-md-6">
				<select name="seccion_id" class="selectpicker" data-live-search="true" 
				title="Seleccione una seccion" data-width="100%" 				
				value='<?php echo $seccion[0]['$seccion_id']; ?>'
				data-none-results-text="No se encontraron resultados" required>
			        <?php
			        	foreach ($valores as $key => $seccion) {
			        		echo '<option value="'.$seccion[id].'" ';
			        			if($accion1[0]['seccion_id'] == $seccion['id']){ 
			        				echo 'selected'; 
			        			}
			        		echo '>'.$seccion['descripcion'].'</option>';
			        	}
			        ?>
			    </select>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary" >Enviar</button>
			</div>
		</div>

	</form>
</div>