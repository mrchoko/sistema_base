<?php
if (!empty($acciones)) {

?>
<div id="accion_lista">
	<div class="container">
		<div class="row">
			<div class="page-header text-center"><h3>Lista</h3></div>
		</div>
	</div>
	<div class="container">
		<div class="row hiden-btn">
			<div class="input-group col-xs-6 col-xs-offset-6">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-1" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
		<div class="row hidden">
			<div class="input-group col-xs-12">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-2" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="table-responsive" id="lista_accion">
					<table class="table table-fixed">
    					<thead>
      						<tr>
      							<th class="col-xs-1">Id</th>
        						<th class="col-xs-4">Descripcion</th>
        						<th class="col-xs-3 hiden-btn">Seccion Descripcion</th>
        						<th class="col-xs-3 hidden">Seccion</th>
        						<th class="col-xs-4 text-center">Acciones</th>
      						</tr>
    					</thead>
    					<tbody class="registros">
						<?php foreach ($acciones as $key => $accion) { ?>
							<tr>
								<td class="col-xs-1"><?php echo $accion['id']; ?></td>
								<td class="col-xs-4 ocultar-texto"><?php echo $accion['descripcion']; ?></td>
								<td class="col-xs-3 ocultar-texto"><?php echo $accion['descripcion_seccion']; ?></td>
								<td class="col-xs-2 hiden-btn">
									<div class="btn-group">
										<a href="index.php?seccion=accion&accion=elimina&accion_id=<?php echo $accion['id']; ?>">
  											<button type="button" class="btn btn-danger">
  												Elimina
  											</button>
  										</a>
  									</div>
  								</td>
  								<td class="col-xs-2 hiden-btn">
  									<div class="btn-group">
  										<a href="index.php?seccion=accion&accion=modifica&accion_id=<?php echo $accion['id']; ?>">
  										<button type="button" class="btn btn-primary">
  											Modifica
  										</button>
  										</a>
									</div>
								</td>


								<td class="col-xs-1 hidden"></td>
								<td class="col-xs-3 text-center hidden">

								    <div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									    	<span>Acciones</span>
									    	<span></span>
									    	<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">
											<li>
												<a href="index.php?seccion=accion&accion=elimina&accion_id=<?php echo $accion['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline">
		  												Elimina
		  											</button>
		  										</a>
											</li>
											<li>
												<a href="index.php?seccion=accion&accion=modifica&accion_id=<?php echo $accion['id']; ?>">
		  										<button type="button" class="btn btn-secondary btn-outline">
		  											Modifica
		  										</button>
		  										</a>
											</li>
										</ul>
									</div>

								</td>


							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}
?>