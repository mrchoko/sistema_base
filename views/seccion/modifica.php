<div id="grupo_alta">
	<div class="page-header text-center"><h4>Modifica</h4></div>
	<form 
		id="form-grupo-alta" name="form-grupo-alta" 
		method="post" 
		action="./index.php?seccion=seccion&accion=modifica_bd&seccion_id=<?php echo $seccion_id; ?>">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-12">
				<input 
					type="text" class="form-control" 
					name="descripcion" placeholder="Ingresa Descripción" 
					required title="Ingrese una descripción" 
					value='<?php echo $seccion1[0]['descripcion']; ?>'>
				<div class="row">&nbsp;</div>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<textarea class="form-control noresize" name="observaciones" rows="3" placeholder="Ingresa Observaciones" title="Ingrese una observacion"><?php echo $seccion1[0]['observaciones']; ?></textarea>
			</div>
		</div>
		<div class="form-group text-center">
			<button type="submit" class="btn btn-secondary" >Enviar</button>
			<input type='hidden' name='status' value='<?php echo $seccion1[0]['status']; ?>'>
		</div>
	</form>
</div>