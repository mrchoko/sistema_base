<?php 

session_start();

if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin'] == true) {
	header('Location: views/session/');
}

$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	header('Location: views/session/');
}

if(!isset($_GET['seccion'])){
	$seccion='session';
	header('Location: views/session/');
}
else{
	$seccion = $_GET['seccion'];
}
if(!isset($_GET['accion'])){
	$accion='inicio';	
}
else{
	$accion = $_GET['accion'];
}
if(!isset($_GET['resultado'])){
	$resultado='';	
}
else{
	$resultado = $_GET['resultado'];
}
if(!isset($_GET['operacion'])){
	$operacion='';	
}
else{
	$operacion = $_GET['operacion'];
}
if(!isset($_GET['token'])){
	$token='';	
}
else{
	$token = $_GET['token'];
}


require_once('Controladores/controlador_'.$seccion.'.php');
require_once('views/mensaje.php');

$name_ctl = 'controlador_'.$_GET['seccion'];
$controlador = new $name_ctl;
if($accion == 'alta_bd'){
	$controlador->alta_bd();
}

if($accion == 'elimina'){
	$controlador->elimina_bd();		
}


?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8" name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, target-densityDpi=device-dpi">
		<title>Control de accesos</title>
		<link rel="stylesheet" href="./views/css/bootstrap.min.css">
  		<link rel="stylesheet" href="./views/css/bootstrap-theme.min.css">
  		<link rel="stylesheet" href="./views/css/layout.css">

  		<link href="./views/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

  		<script src="./views/js/jquery.min.js"></script>	
  		<script src="./views/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="./views/js/funciones.js"></script>

		 

		<script type='text/javascript'>
			$(document).ready(function () {
			<?php if(!empty($operacion) && !empty($resultado)) { ?>
				$('#<?php echo $resultado.''.$operacion; ?>').modal('show'); 
				history.pushState(null,'','<?php echo 'index.php?seccion='.$seccion.'&accion='.$accion ?>');
			<?php } ?>
			});
		</script>  
	</head>
	<body>
		<div class="row center-aligned">
			<div class="col-md-2 text-center">
				<img  class="logo" src="./views/img/logo.svg">		
			</div>
			<div class="col-md-10 text-center">
				<h1>Control </h1>	
			</div>
		</div>
		<div class="row affix-row">
			<div class="col-md-2 affix-sidebar">
				<div class="sidebar-nav">
					<div class="navbar navbar-default" role="navigation"><!-- aqui -->
						<div class="navbar-header">
						    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse" >
							    <span class="sr-only"></span>
							    <span class="icon-bar"></span>
							    <span class="icon-bar"></span>
							    <span class="icon-bar"></span>
						    </button>
						    <span class="visible-xs navbar-brand">Menu</span>
					    </div>
						<div class="navbar-collapse collapse sidebar-navbar-collapse" id="nav-main">
							<ul class="nav navbar-nav" id="menu">
								<li class="active">
									<a data-toggle="tab" href="#"><h4>Panel de Control<br></h4></a>
								</li>
								<li>
									<a id="a-grupo" href="#" data-toggle="collapse" data-target="#grupo" data-parent="#menu" class="collapsed">
										<span class="glyphicon glyphicon-cloud"></span> Grupo <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="grupo" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=grupo&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=grupo&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a href="#" data-toggle="collapse" data-target="#seccion" data-parent="#menu" class="collapsed">
										<span class="glyphicon glyphicon-inbox"></span> Sección <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="seccion" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=seccion&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=seccion&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a href="#" data-toggle="collapse" data-target="#usuario" data-parent="#menu" class="collapsed">
										<span class="glyphicon glyphicon-lock"></span> Usuario <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="usuario" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=usuario&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=usuario&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a href="#" data-toggle="collapse" data-target="#accion" data-parent="#menu" class="collapsed">
										<span class="glyphicon glyphicon-wrench"></span> Acción <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="accion" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=accion&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=accion&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a href="#" data-toggle="collapse" data-target="#accion_grupo" data-parent="#menu" class="collapsed">
										<span class="glyphicon glyphicon-cog"></span> Acción Grupo <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="accion_grupo" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=accion_grupo&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=accion_grupo&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a href="#" data-toggle="collapse" data-target="#session" data-parent="#menu" class="collapsed">
										<span class="glyphicon glyphicon-cog"></span> Sesión <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="session" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="./views/session/logout.php"><p>Cerrar Sesión</p></a></li>
										</ul>
									</div>
								</li>
								

							</ul>
						</div>
					</div>

				</div>
			</div>

			<div class="col-md-10 affix-content">
				<div class="container">
					<div class="tab-content">

						<div id="home" class="tab-pane fade in active">
							<div class="page-header text-center">
								<h3><?php echo $seccion; ?></h3>
							</div>
						</div>

						<?php 
							include('./views/'.$seccion.'/'.$accion.'.php'); 
						?>

					</div>	
				</div>
			</div>

		</div>

		<?php $mensaje_controller->genera_mensaje($resultado,$operacion); ?>

		<?php 
			include('./views/zoom_foto.php'); 
		?>

		<link href="./views/css/bootstrap-select.min.css" rel="stylesheet"/>
		<script src="./views/js/bootstrap-select.min.js"></script>
  		

		<script src="./views/js/moment.min.js"></script>
		<script src="./views/js/bootstrap-datetimepicker.min.js"></script>
		<script src="./views/js/bootstrap-datetimepicker.es.js"></script>
		<script type="text/javascript">
		    $('#divMiCalendario').datetimepicker({
		        format: 'YYYY-MM-DD',
		        autoclose: true     
		    });
		</script>

	</body>
</html>