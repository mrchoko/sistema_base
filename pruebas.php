<?php
require_once('./Controladores/lib/nusoap.php');
require_once "config/init.php";
require_once "modelos/grupo.php";
require_once "modelos/seccion.php";
require_once "modelos/usuario.php";
require_once "modelos/accion.php";
require_once "modelos/accion_grupo.php";

class Pruebas{
	public function finaliza_prueba($funcion){
		echo "<br>----------------------Finaliza Prueba----".$funcion."------------------------<br><br><br>";
	}	
	public function inicia_prueba($funcion){
		echo "<br>----------------------Inicia Prueba----".$funcion."---------------------------------<br>";
	}	
	public function lineas($mensaje,$linea,$archivo){
		echo "<br>-----".$mensaje."-----Linea-----".$linea."---".$archivo."--</br>";
	}	

	public function test_config_conexion_selecciona_base_datos(){ //finalizada
		$funcion = 'test_config_conexion_selecciona_base_datos';
		$this->inicia_prueba($funcion);
		$nombre_base_datos = 'test_1';
		$conexion = new Conexion();
		$bool = $conexion->selecciona_base_datos($nombre_base_datos);
		if($bool){
			$this->lineas('Error '.$funcion, __LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion, __LINE__,__FILE__);	
		}
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);
		$bool = $conexion->selecciona_base_datos($nombre_base_datos);
		if(!$bool){
			$this->lineas('Error '.$funcion, __LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion, __LINE__,__FILE__);	
		}
		$init->elimina_base_datos($nombre_base_datos);

		$this->finaliza_prueba($funcion);
	}

	public function test_config_init_crea_base_datos(){ //finalizada
		$funcion = 'test_config_init_crea_base_datos';
		$this->inicia_prueba($funcion);
		$nombre_base_datos = 'test_1';

		$init = new Init();
		$bool = $init->crea_base_datos($nombre_base_datos);
		if(!$bool){
			$this->lineas('Error '.$funcion, __LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion, __LINE__,__FILE__);	
		}
		$init->elimina_base_datos($nombre_base_datos);

		$init->crea_base_datos($nombre_base_datos);
		$bool = $init->crea_base_datos($nombre_base_datos);
		if($bool){
			$this->lineas('Error '.$funcion, __LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion, __LINE__,__FILE__);	
		}
		$init->elimina_base_datos($nombre_base_datos);


		$this->finaliza_prueba($funcion);
	}



	public function test_modelos_activa_db(){ // En proceso
		$funcion = 'test_modelos_activa_db';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');

		$modelo = new Modelos();
		$bool = $modelo->activa_db(1, 'grupo');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_alta_db(){ // En proceso
		$funcion = 'test_modelos_alta_db';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$conexion->link->query('CREATE TABLE  grupo(
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descripcion VARCHAR(500) NOT NULL UNIQUE,
	observaciones TEXT,
	status TINYINT(1) NOT NULL);');

		$registro = array('descripcion' => 'Grupo A', 'observaciones' => 'Grupo A 2013', 'status' => 1);
		$registro_campo_invalido = array('campo_invalido' => 'Grupo A', 'status' => 1);
		$registro_nulo_invalido = array('descripcion' => '', 'observaciones' => 'Grupo A 2013', 'status' => 1);
		$registro_sin_campo_obligatorio = array('observaciones' => 'Grupo A 2013', 'status' => 1);

		$modelo = new Modelos();
		$bool = $modelo->alta_db(1,'grupo', $nombre_base_datos);
		if($bool){
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}

		$bool = $modelo->alta_db($registro,'grupos',$nombre_base_datos);
		if(!$bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$bool = $modelo->alta_db($registro_campo_invalido,'grupo',$nombre_base_datos);
		if(!$bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}		

		$bool = $modelo->alta_db($registro_nulo_invalido,'grupo',$nombre_base_datos);
		if(!$bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}		

		$bool = $modelo->alta_db($registro_sin_campo_obligatorio,'grupo',$nombre_base_datos);
		if(!$bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}		

		$bool = $modelo->alta_db($registro,'grupo',$nombre_base_datos);
		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}


		$init->elimina_base_datos($nombre_base_datos); 
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_desactiva_db(){ // En proceso
		$funcion = 'test_modelos_desactiva_db';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');

		$modelo = new Modelos();
		$bool = $modelo->activa_db(1, 'grupo');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_modifica_db(){ // En proceso
		$funcion = 'test_modelos_modifica_db';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$registro = array(
			'id'=>'1', 'descripcion' => 'Grupo B', 'observaciones' => 'Grupo B 2016', 'status' => 0);

		$modelo = new Modelos();

		$bool = $modelo->modifica_db($registro,'grupo');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_grupo_activa(){
		$funcion = 'test_modelos_grupo_activa';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');

		$grupo = new Grupo();
		$bool = $grupo->activa_db(1, 'grupo');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_grupo_alta(){
		$funcion = 'test_modelos_grupo_alta';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');

		$registro = array('descripcion' => 'Grupo A', 'observaciones' => 'Grupo A 2013', 'status' => 1);

		$grupo = new Grupo();
		$bool = $grupo->alta_db($registro,'grupo',$nombre_base_datos);
		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		
		$init->elimina_base_datos($nombre_base_datos);
		$this->finaliza_prueba($funcion);
	}

	
	public function test_modelos_grupo_desactiva(){
		$funcion = 'test_modelos_grupo_desactiva';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');

		$grupo = new Grupo();
		$bool = $grupo->desactiva_db(1, 'grupo');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_grupo_elimina(){
		$funcion = 'test_modelos_grupo_elimina';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');

		$grupo = new Grupo();
		$bool = $grupo->elimina_db('grupo',1,$nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_grupo_modifica(){ // En proceso
		$funcion = 'test_modelos_grupo_modifica';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$registro = array(
			'id'=>'1', 'descripcion' => 'Grupo BS', 'observaciones' => 'Grupo BS 2016', 'status' => 1);

		$grupo = new Grupo();

		$bool = $grupo->modifica_db($registro,'grupo',"test_1");

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		//$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_grupo_obten_por_id(){ // En proceso
		$funcion = 'test_modelos_grupo_obten_por_id';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$conexion->link->query('CREATE TABLE  grupo(
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descripcion VARCHAR(500) NOT NULL UNIQUE,
	observaciones TEXT,
	status TINYINT(1) NOT NULL);');

		$registro1 = array(
			'descripcion' => 'Grupo AAA', 'observaciones' => 'Grupo A 2013', 'status' => 1);

		$registro2 = array(
			'descripcion' => 'Grupo BBB', 'observaciones' => 'Grupo A 2013', 'status' => 1);


		$grupo = new Grupo();

		$id_1 = $grupo->alta_db($registro1,'grupo',$nombre_base_datos);
		$id_2 = $grupo->alta_db($registro2,'grupo',$nombre_base_datos);

		$bool = $grupo->obten_por_id('grupo', $id_1, $nombre_base_datos);

		if($bool){
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}



		$bool = $grupo->obten_por_id('grupo', $id_2, $nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		if(is_array($bool)){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}


		$n_registros = count($bool);

		if($n_registros == 1){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);	
		}

		if(array_key_exists('id', $bool[0])){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);		
		}

		$init->elimina_base_datos($nombre_base_datos); 
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_obten_por_id(){ // En proceso
		$funcion = 'test_modelos_obten_por_id';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$conexion->link->query('CREATE TABLE  grupo(
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descripcion VARCHAR(500) NOT NULL UNIQUE,
	observaciones TEXT,
	status TINYINT(1) NOT NULL);');

		$registro1 = array(
			'descripcion' => 'Grupo A', 'observaciones' => 'Grupo A 2013', 'status' => 1);

		$registro2 = array(
			'descripcion' => 'Grupo B', 'observaciones' => 'Grupo A 2013', 'status' => 1);


		$modelo = new Modelos();

		$id_1 = $modelo->alta_db($registro1,'grupo',$nombre_base_datos);
		$id_2 = $modelo->alta_db($registro2,'grupo',$nombre_base_datos);

		$bool = $modelo->obten_por_id('tabla_inexistente', $id_1, $nombre_base_datos);

		if($bool){
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}



		$bool = $modelo->obten_por_id('grupo', $id_1, $nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		if(is_array($bool)){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}


		$n_registros = count($bool);

		if($n_registros == 1){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);	
		}

		if(array_key_exists('id', $bool[0])){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);		
		}

		$init->elimina_base_datos($nombre_base_datos); 
		$this->finaliza_prueba($funcion);
	}	


	//---------------- Inicia Seccion ------------------------------

	public function test_modelos_seccion_activa(){
		$funcion = 'test_modelos_seccion_activa';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$seccion = new Seccion();
		$bool = $seccion->activa_db(1, 'seccion');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_seccion_alta(){
		$funcion = 'test_modelos_seccion_alta';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		/*$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$registro = array('descripcion' => 'Grupo A', 'observaciones' => 'Grupo A 2013', 'status' => 1);

		$seccion = new Seccion();
		$bool = $seccion->alta_db($registro,'seccion',$nombre_base_datos);
		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		
		//$init->elimina_base_datos($nombre_base_datos);
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_seccion_desactiva(){
		$funcion = 'test_modelos_seccion_desactiva';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		/*$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$seccion = new Seccion();
		$bool = $seccion->desactiva_db(1, 'seccion');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_seccion_elimina(){
		$funcion = 'test_modelos_seccion_elimina';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		/*$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$seccion = new Seccion();
		$bool = $seccion->elimina_db('seccion',1,$nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_seccion_modifica(){ // En proceso
		$funcion = 'test_modelos_seccion_modifica';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$registro = array(
			'id'=>'1', 'descripcion' => 'Grupo B', 'observaciones' => 'Grupo B 2016', 'status' => 0);

		$seccion = new Seccion();

		$bool = $seccion->modifica_db($registro,'seccion');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_seccion_obten_por_id(){ // En proceso
		$funcion = 'test_modelos_seccion_obten_por_id';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo(
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descripcion VARCHAR(500) NOT NULL UNIQUE,
	observaciones TEXT,
	status TINYINT(1) NOT NULL);');*/

		$registro1 = array(
			'descripcion' => 'Grupo AA', 'observaciones' => 'Grupo A 2013', 'status' => 1);

		$registro2 = array(
			'descripcion' => 'Grupo B', 'observaciones' => 'Grupo A 2013', 'status' => 1);


		$seccion = new Seccion();

		$id_1 = $seccion->alta_db($registro1,'seccion',$nombre_base_datos);
		$id_2 = $seccion->alta_db($registro2,'seccion',$nombre_base_datos);

		$bool = $seccion->obten_por_id('seccion', $id_1, $nombre_base_datos);

		if($bool){
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}



		$bool = $seccion->obten_por_id('seccion', $id_2, $nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		if(is_array($bool)){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}


		$n_registros = count($bool);

		if($n_registros == 1){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);	
		}

		if(array_key_exists('id', $bool[0])){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);		
		}

		$init->elimina_base_datos($nombre_base_datos); 
		$this->finaliza_prueba($funcion);
	}

	//----------------- Fin Seccion ---------------------------------

	//----------------- Inicio Usuario ------------------------------

	public function test_modelos_usuario_alta(){
		$funcion = 'test_modelos_usuario_alta';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		/*$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/
		//user,password,email,grupo_id
		$registro = array('user' => 'noyola', 'password' => '1qa2ws3ed', 'email' => 'jnoyola@tdesystems.com.mx', 'grupo_id' => 1);

		$usuario = new Usuario();
		$bool = $usuario->alta_db($registro,'usuario',$nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		
		$init->elimina_base_datos($nombre_base_datos);
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_usuario_elimina(){
		$funcion = 'test_modelos_usuario_elimina';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		/*$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$usuario = new Usuario();
		$bool = $usuario->elimina_db('usuario',1,$nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_usuario_modifica(){ // En proceso
		$funcion = 'test_modelos_usuario_modifica';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/
		$registro = array('id'=>'1','user' => 'macias', 'password' => '1qa2ws3ed4rf', 'email' => 'jnoyola@tdesystems.com.mx', 'grupo_id' => 1);

		$usuario = new Usuario();

		$bool = $usuario->modifica_db($registro,'usuario');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_usuario_obten_por_id(){ // En proceso
		$funcion = 'test_modelos_usuario_obten_por_id';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo(
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descripcion VARCHAR(500) NOT NULL UNIQUE,
	observaciones TEXT,
	status TINYINT(1) NOT NULL);');*/

		$registro1 = array('user' => 'lili', 'password' => '112345', 'email' => '1levangelista@tdesystems.com.mx', 'grupo_id' => 1);
		$registro2 = array('user' => 'choko', 'password' => 'qwerty', 'email' => '1elnino@tdesystems.com.mx', 'grupo_id' => 1);

		$usuario = new Usuario();

		$id_1 = $usuario->alta_db($registro1,'usuario',$nombre_base_datos);
		$id_2 = $usuario->alta_db($registro2,'usuario',$nombre_base_datos);

		$bool = $usuario->obten_por_id('usuario', $id_1, $nombre_base_datos);
		
		if($bool){
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}



		$bool = $usuario->obten_por_id('usuario', $id_2, $nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		if(is_array($bool)){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}


		$n_registros = count($bool);

		if($n_registros == 1){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);	
		}

		if(array_key_exists('id', $bool[0])){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);		
		}

		$init->elimina_base_datos($nombre_base_datos); 
		$this->finaliza_prueba($funcion);
	}

	//----------------- Fin Usuario ---------------------------------

	//----------------- Inicio Accion -------------------------------

	public function test_modelos_accion_alta(){
		$funcion = 'test_modelos_accion_alta';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		/*$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/
		//'id'=>$id, 'descripcion'=>$descripcion, 'seccion_id'=>$seccion_id
		$registro = array('descripcion'=>'Reporte', 'seccion_id'=>1);

		$accion = new Accion();
		$bool = $accion->alta_db($registro,'accion',$nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		
		$init->elimina_base_datos($nombre_base_datos);
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_accion_elimina(){
		$funcion = 'test_modelos_accion_elimina';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		/*$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$accion = new Accion();
		$bool = $accion->elimina_db('accion',1,$nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_accion_modifica(){ // En proceso
		$funcion = 'test_modelos_accion_modifica';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$registro = array('id'=>1,'descripcion'=>'Administrador', 'seccion_id'=>1);

		$accion = new Accion();

		$bool = $accion->modifica_db($registro,'accion');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_accion_obten_por_id(){ // En proceso
		$funcion = 'test_modelos_accion_obten_por_id';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo(
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descripcion VARCHAR(500) NOT NULL UNIQUE,
	observaciones TEXT,
	status TINYINT(1) NOT NULL);');*/

		$registro1 = array('descripcion'=>'Reporte 5', 'seccion_id'=>1);
		$registro2 = array('descripcion'=>'Reporte 4', 'seccion_id'=>1);

		$accion = new Accion();

		$id_1 = $accion->alta_db($registro1,'accion',$nombre_base_datos);
		$id_2 = $accion->alta_db($registro2,'accion',$nombre_base_datos);

		$bool = $accion->obten_por_id('accion', $id_1, $nombre_base_datos);
		
		if($bool){
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}



		$bool = $accion->obten_por_id('accion', $id_2, $nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		if(is_array($bool)){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}


		$n_registros = count($bool);

		if($n_registros == 1){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);	
		}

		if(array_key_exists('id', $bool[0])){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);		
		}

		$init->elimina_base_datos($nombre_base_datos); 
		$this->finaliza_prueba($funcion);
	}	

	//------------------- Fin Accion --------------------------------

	//----------------- Inicio Accion_Grupo -------------------------------

	public function test_modelos_accion_grupo_alta(){
		$funcion = 'test_modelos_accion_grupo_alta';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		/*$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/
		//'id'=>$id, 'descripcion'=>$descripcion, 'seccion_id'=>$seccion_id
		$registro = array('accion_id'=>2, 'grupo_id'=>1);

		$accion_grupo = new Accion_Grupo();
		$bool = $accion_grupo->alta_db($registro,'accion_grupo',$nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		
		$init->elimina_base_datos($nombre_base_datos);
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_accion_grupo_elimina(){
		$funcion = 'test_modelos_accion_grupo_elimina';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		/*$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$accion_grupo = new Accion_Grupo();
		$bool = $accion_grupo->elimina_db('accion_grupo',1,$nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_accion_grupo_modifica(){ // En proceso
		$funcion = 'test_modelos_accion_grupo_modifica';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo( 
			id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,descripcion VARCHAR(500) NOT NULL UNIQUE,
			observaciones TEXT, status TINYINT(1) NOT NULL);');*/

		$registro = array('id'=>1,'accion_id'=>3, 'grupo_id'=>1);

		$accion_grupo = new Accion_Grupo();

		$bool = $accion_grupo->modifica_db($registro,'accion_grupo');

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		$init->elimina_base_datos($nombre_base_datos);
	
		$this->finaliza_prueba($funcion);
	}

	public function test_modelos_accion_grupo_obten_por_id(){ // En proceso
		$funcion = 'test_modelos_accion_grupo_obten_por_id';
		$this->inicia_prueba($funcion);

		$nombre_base_datos = 'test_1';
		$init = new Init();
		$init->crea_base_datos($nombre_base_datos);		

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
/*
		$conexion->link->query('CREATE TABLE  grupo(
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descripcion VARCHAR(500) NOT NULL UNIQUE,
	observaciones TEXT,
	status TINYINT(1) NOT NULL);');*/

		$registro1 = array('accion_id'=>3, 'grupo_id'=>1);
		$registro2 = array('accion_id'=>2, 'grupo_id'=>1);

		$accion_grupo = new Accion_Grupo();

		$id_1 = $accion_grupo->alta_db($registro1,'accion_grupo',$nombre_base_datos);
		$id_2 = $accion_grupo->alta_db($registro2,'accion_grupo',$nombre_base_datos);

		$bool = $accion_grupo->obten_por_id('accion_grupo', $id_1, $nombre_base_datos);
		
		if($bool){
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}
		else{
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}



		$bool = $accion_grupo->obten_por_id('accion_grupo', $id_2, $nombre_base_datos);

		if($bool){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}

		if(is_array($bool)){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);	
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);
		}


		$n_registros = count($bool);

		if($n_registros == 1){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);	
		}

		if(array_key_exists('id', $bool[0])){
			$this->lineas('Exito '.$funcion,__LINE__,__FILE__);		
		}
		else{
			$this->lineas('Error '.$funcion,__LINE__,__FILE__);		
		}

		$init->elimina_base_datos($nombre_base_datos); 
		$this->finaliza_prueba($funcion);
	}	

	//------------------- Fin Accion_Grupo --------------------------------

	//---------------- Inicio Pruebas Controlador Base -----------------------


	public function test_controladores_controlador_base_activa(){// en proceso 


		$cliente = new nusoap_client('http://localhost/sistema_base/Controladores/controlador_base.php');
		$resultado = $cliente->call('Controlador_Base.activa', 
			array(
				'id' => 16, 
			'tabla' => 'grupo', 
			'nombre_base_datos' => 'test_1'));
		print_r( $resultado);
	}



	public function test_controladores_controlador_base_desactiva(){// en proceso 

		$cliente = new nusoap_client('http://localhost/sistema_base/Controladores/controlador_base.php');
		$resultado = $cliente->call('Controlador_Base.desactiva', 
			array(
				'id' => 16, 
			'tabla' => 'grupo', 
			'nombre_base_datos' => 'test_1'));
		print_r( $resultado);
	}



	public function test_controladores_controlador_base_elimina(){// en proceso 

		$cliente = new nusoap_client('http://localhost/sistema_base/Controladores/controlador_base.php');
		$resultado = $cliente->call('Controlador_Base.elimina', 
			array(
				'id' => 17, 
			'tabla' => 'grupo', 
			'nombre_base_datos' => 'test_1'));
		print_r( $resultado);
		

	}

	public function test_controladores_controlador_base_inserta(){// en proceso 
		
		$descripcion = 'xxx';
		$observaciones = 'xxx';
		$status = '0';
		//print_r($_POST);
		//echo $descripcion.'-'.$observaciones.'-'.$status;
		$cliente = new nusoap_client('http://localhost/sistema_base/Controladores/controlador_base.php');
		$resultado = $cliente->call('Controlador_Base.inserta', 
			array(
				'registro' => array('descripcion' => $descripcion, 
								'observaciones' => $observaciones, 
								'status' => $status), 
			'tabla' => 'grupo', 
			'nombre_base_datos' => 'test_1'));
		print_r($resultado);
	}

	public function test_controladores_controlador_grupo_inserta(){// en proceso 
		
		$descripcion = 'xxx';
		$observaciones = 'xxx';
		$status = '1';
		//print_r($_POST);
		//echo $descripcion.'-'.$observaciones.'-'.$status;
		$cliente = new nusoap_client('http://localhost/sistema_base/Controladores/controlador_grupo.php');

		$resultado = $cliente->call('Controlador_Grupo.inserta', 
			array(
				'registro' => array('descripcion' => $descripcion, 
								'observaciones' => $observaciones, 
								'status' => $status), 
			'tabla' => 'grupo', 
			'nombre_base_datos' => 'test_1'));
		//print_r($resultado);
	}
	public function test_controladores_controlador_mpt_inserta(){// en proceso 
		
		$descripcion = 'xxx';
		$observaciones = 'xxx';
		$status = '1';
		//print_r($_POST);
		//echo $descripcion.'-'.$observaciones.'-'.$status;
		$cliente = new nusoap_client('http://localhost/sistema_base/Controladores/controlador_grupo.php');

		$resultado = $cliente->call('Controlador_Grupo.inserta', 
			array(
				'registro' => array('descripcion' => $descripcion, 
								'observaciones' => $observaciones, 
								'status' => $status), 
			'tabla' => 'grupo', 
			'nombre_base_datos' => 'test_1'));
		//print_r($resultado);
	}


	public function test_controladores_controlador_base_modifica(){// en proceso 
		
		$cliente = new nusoap_client('http://localhost/sistema_base/Controladores/controlador_base.php');
		$resultado = $cliente->call('Controlador_Base.modifica', 
			array(
				'registro' => array('id' => 18,'descripcion' => 'Grupo TYTYTY', 'observaciones' => 'Grupo TYTYTY 2013', 'status' => 1), 
			'tabla' => 'grupo', 
			'nombre_base_datos' => 'test_1'));
		print_r( $resultado);


	}

	//---------------- Fin Pruebas Controlador Base ---------------------------


	public function test_modelos_genera_lista_grupo(){
        $modelo = new Modelos();
        $resultado = $modelo->genera_lista_grupo();
        print_r($resultado);
    } 

    public function test_controladores_controlador_base_lista_grupo(){
        $cliente = new nusoap_client('http://localhost/sistema_base/Controladores/controlador_base.php');
		$resultado = $cliente->call('Controlador_Base.lista_grupo', 
			array('nombre_base_datos' => 'test_1'));
		print_r( $resultado);
    }
}

$pruebas = new Pruebas();
/*
$pruebas->test_config_conexion_selecciona_base_datos();
$pruebas->test_config_init_crea_base_datos();
$pruebas->test_modelos_alta_db();
$pruebas->test_modelos_modifica_db();*/
//$pruebas->test_modelos_obten_por_id();
//$pruebas->test_modelos_grupo_alta();
//$pruebas->test_modelos_grupo_modifica();
//$pruebas->test_modelos_grupo_activa();
//$pruebas->test_modelos_grupo_desactiva();
//$pruebas->test_modelos_grupo_elimina();
//$pruebas->test_modelos_grupo_obten_por_id();

//$pruebas->test_modelos_seccion_alta();
//$pruebas->test_modelos_seccion_modifica();
//$pruebas->test_modelos_seccion_activa();
//$pruebas->test_modelos_seccion_desactiva();
//$pruebas->test_modelos_seccion_elimina();
//$pruebas->test_modelos_seccion_obten_por_id();

//$pruebas->test_modelos_usuario_alta();
//$pruebas->test_modelos_usuario_modifica();
//$pruebas->test_modelos_usuario_elimina();
//$pruebas->test_modelos_usuario_obten_por_id();

//$pruebas->test_modelos_accion_alta();
//$pruebas->test_modelos_accion_modifica();
//$pruebas->test_modelos_accion_elimina();
//$pruebas->test_modelos_accion_obten_por_id();

//$pruebas->test_modelos_accion_grupo_alta();
//$pruebas->test_modelos_accion_grupo_modifica();
//$pruebas->test_modelos_accion_grupo_elimina();
//$pruebas->test_modelos_accion_grupo_obten_por_id();

$pruebas->test_modelos_mpt_alta();

//$pruebas->test_controladores_controlador_base_activa();
//$pruebas->test_controladores_controlador_base_desactiva();
//$pruebas->test_controladores_controlador_base_elimina();
//$pruebas->test_controladores_controlador_base_inserta();
//$pruebas->test_controladores_controlador_base_modifica();
//$pruebas->test_config_init_crea_estructura();
//seccion

//$pruebas->test_modelos_genera_lista_grupo();
//$pruebas->test_controladores_controlador_base_lista_grupo();

?>