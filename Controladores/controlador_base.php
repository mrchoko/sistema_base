<?php
if(file_exists('../config/conexion.php')){
	require_once('../config/conexion.php');
}
if(file_exists('../modelos.php')){
	require_once('../modelos.php');
}


class Controlador_Base{

	public function activa($id=False,$tabla=False,$nombre_base_datos=False){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$modelo = new Modelos();
		$resultado = $modelo->activa_db($id,$tabla);

		return $resultado;
	}

	public function desactiva($id,$tabla,$nombre_base_datos){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$modelo = new Modelos();
		$resultado = $modelo->desactiva_db($id,$tabla);

		return $resultado;

	}

	public function redirect($registro, $accion, $tabla,$operacion){

		if($registro){
			header('Location: index.php?seccion='.$tabla.'&accion='.$accion.'&resultado=correcto&operacion='.$operacion);
		}
		else{
			header('Location: index.php?seccion='.$tabla.'&accion='.$accion.'&resultado=incorrecto&operacion='.$operacion);	
		}

	}

	public function desactiva_db(){
		$tabla = $_GET['seccion'];
		$registro_id = $_GET[$tabla.'_id'];

		$registro = $this->desactiva($registro_id, $tabla);		
	}	

	public function elimina($id,$tabla){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();

		$modelo = new Modelos();
		$modelo->elimina_db($tabla,$id);
		$registro_obtenido = $modelo->obten_por_id($tabla,$id);
		
		if (!$registro_obtenido) {
			return true;
		}
		else{
			return false;
		}

	}

	public function elimina_bd(){
		$tabla = $_GET['seccion'];
		$registro_id = $_GET[$tabla.'_id'];

		$registro = $this->elimina($registro_id,$tabla);
		$this->redirect($registro, 'lista', $tabla,'Elimina');

	}	

	public function inserta($registro,$tabla,$nombre_base_datos=false){
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$modelo = new Modelos();
		$registro_id = $modelo->alta_db($registro,$tabla,$nombre_base_datos);
		if($registro_id){
			$registro_obtenido = $modelo->obten_por_id($tabla,$registro_id,$nombre_base_datos);
			$registro_enviar[$tabla] = $registro_obtenido;
			return $registro_enviar;
		}
		else{
			return false;
		}
	}
	public function modifica($registro,$tabla,$nombre_base_datos){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);

		$modelo = new Modelos();
		$registro_id = $modelo->modifica_db($registro,$tabla);

		if($registro_id){
			$registro_obtenido = $modelo->obten_por_id($tabla,$registro_id,$nombre_base_datos);
			$registro_enviar[$tabla] = $registro_obtenido;
			return $registro_enviar;
		}
		else{
			return false;
		}
	}

	public function alta_bd(){
		$campos = $this->obten_estructura($_GET['seccion']);
		$registro = $this->genera_registro_alta($campos);
		$tabla = $this->inserta($registro,$_GET['seccion']);
		$this->redirect($tabla, 'lista', $_GET['seccion'],'Guarda');
	}

	public function genera_registro_alta($campos_grupo){
		foreach ($campos_grupo as $key => $campo) {
			if($key!='id'){
				if($key=='status'){
					$registro[$key] = (isset($_POST['status'])) ? 1 : 0;
				}
				else{
					$registro[$key] = $_POST[$key];	
				}
			}
		}
		return $registro;
	}

	public function obten_estructura($tabla){
		$modelo = new modelos();
		$db = $modelo->genera_estructura();
		$estructura_grupo = $db[$tabla];
		$campos_grupo = $estructura_grupo['campos'];
		return $campos_grupo;
	}
}
?>