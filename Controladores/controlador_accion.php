<?php
require_once('controlador_base.php');
require_once('controlador_seccion.php');
if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Accion extends Controlador_Base{
	public function lista_accion(){
		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_accion();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}

}


$accion_controller = new Controlador_Accion();


if($accion == 'alta' || $accion == 'modifica'){
	$controller_seccion = new Controlador_Seccion();
	$valores = $controller_seccion->lista_seccion();
}

if($accion == 'lista'){
	$acciones = $accion_controller->lista_accion();
}


if($accion == 'modifica' && $seccion == 'accion'){
	$accion_id = $_GET['accion_id'];
	$conexion = new Conexion();
	$conexion->selecciona_base_datos();
	$modelo = new Modelos();
	$accion1 = $modelo->obten_por_id('accion',$accion_id);
}

if($accion == 'modifica_bd' ){
	$accion_id = $_GET['accion_id'];
	$descripcion = $_POST['descripcion'];
	$seccion_id = $_POST['seccion_id'];

	$registro = array(
		'id'=>$accion_id,'descripcion'=>$descripcion,'seccion_id'=>$seccion_id);
	$tabla = 'accion';
	$accion1 = $accion_controller->modifica($registro,$tabla,$nombre_base_datos);

	if($accion1){
		header('Location: index.php?seccion=accion&accion=modifica&resultado=correcto&operacion=Modifica&accion_id='.$accion_id);
	}
	else{
		header('Location: index.php?seccion=accion&accion=modifica&resultado=incorrecto&operacion=Modifica&accion_id='.$accion_id);	
	}
}
?>