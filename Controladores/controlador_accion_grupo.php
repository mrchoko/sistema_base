<?php
require_once('controlador_base.php');
require_once('controlador_accion.php');
require_once('controlador_grupo.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Accion_Grupo extends Controlador_Base{

	public function lista_accion_grupo($nombre_base_datos){
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_accion_grupo();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}

}

$accion_grupo_controller = new Controlador_Accion_Grupo();
$nombre_base_datos = 'test';


if($accion == 'alta' || $accion == 'modifica'){
	$controller_accion = new Controlador_Accion();
	$valores1 = $controller_accion->lista_accion('nombre_base_datos');
	$controller_grupo = new Controlador_Grupo();
	$valores2 = $controller_grupo->lista_grupo('nombre_base_datos');
}

if($accion == 'alta_db'){
	$accion_id = $_POST['accion_id'];
	$grupo_id = $_POST['grupo_id'];

	$registro = array(
		'accion_id'=>$accion_id, 
		'grupo_id'=>$grupo_id);
	$tabla = 'accion_grupo';

	$accion_grupo = $accion_grupo_controller->inserta($registro,$tabla,$nombre_base_datos);

	if($accion_grupo){
		header('Location: index.php?seccion=accion_grupo&accion=alta&resultado=correcto&operacion=Guarda');
	}
	else{
		header('Location: index.php?seccion=accion_grupo&accion=alta&resultado=incorrecto&operacion=Guarda');	
	}

}
if($accion == 'lista'){
	$accion_grupo = $accion_grupo_controller->lista_accion_grupo($nombre_base_datos);
}
if($accion == 'elimina'){
	$accion_grupo_id = $_GET['accion_grupo_id'];
	$accion_grupo = $accion_grupo_controller->elimina($accion_grupo_id,'accion_grupo',$nombre_base_datos);
	if($accion_grupo){
		header('Location: index.php?seccion=accion_grupo&accion=lista&resultado=correcto&operacion=Elimina');
	}
	else{
		header('Location: index.php?seccion=accion_grupo&accion=lista&resultado=incorrecto&operacion=Elimina');	
	}		
}


if($accion == 'modifica'){
	$accion_grupo_id = $_GET['accion_grupo_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos($nombre_base_datos);

	$modelo = new Modelos();

	$accion_grupo = $modelo->obten_por_id('accion_grupo',$accion_grupo_id ,$nombre_base_datos);

}
if($accion == 'modifica_bd' ){
	$accion_grupo_id = $_GET['accion_grupo_id'];
	$accion_id = $_POST['accion_id'];
	$grupo_id = $_POST['grupo_id'];

	$registro = array(
		'id'=>$accion_grupo_id,'accion_id'=>$accion_id,'grupo_id'=>$grupo_id);
	$tabla = 'accion_grupo';
	$accion_grupo = $accion_grupo_controller->modifica($registro,$tabla,$nombre_base_datos);

	if($accion_grupo){
		header('Location: index.php?seccion=accion_grupo&accion=modifica&resultado=correcto&operacion=Modifica&accion_grupo_id='.$accion_grupo_id);
	}
	else{
		header('Location: index.php?seccion=accion_grupo&accion=modifica&resultado=incorrecto&operacion=Modifica&accion_grupo_id='.$accion_grupo_id);	
	}
}
?>