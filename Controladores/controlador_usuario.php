<?php
require_once('controlador_base.php');
require_once('controlador_grupo.php');
if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Usuario extends Controlador_Base{
	public function lista_usuario(){
		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_usuario();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}

}


$usuario_controller = new Controlador_Usuario();


if($accion == 'alta' || $accion == 'modifica'){
	$controller_grupo = new Controlador_Grupo();
	$valores = $controller_grupo->lista_grupo();
}

if($accion == 'lista'){
	$usuarios = $usuario_controller->lista_usuario();
}
if($accion == 'modifica' ){
	$usuario_id = $_GET['usuario_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos();

	$modelo = new Modelos();

	$usuario = $modelo->obten_por_id('usuario',$usuario_id);

}
if($accion == 'modifica_bd' ){
	$usuario_id = $_GET['usuario_id'];
	$user = $_POST['usuario'];
	$password = $_POST['contrasena'];
	$email = $_POST['correo'];
	$grupo_id = $_POST['grupo_id'];

	$registro = array(
		'id'=>$usuario_id,'user'=>$user, 'password'=>$password, 'email'=>$email,'grupo_id'=>$grupo_id);
	$tabla = 'usuario';
	$usuario = $usuario_controller->modifica($registro,$tabla);

	if($usuario){
		header('Location: index.php?seccion=usuario&accion=modifica&resultado=correcto&operacion=Modifica&usuario_id='.$usuario_id);
	}
	else{
		header('Location: index.php?seccion=usuario&accion=modifica&resultado=incorrecto&operacion=Modifica&usuario_id='.$usuario_id);	
	}
}
?>