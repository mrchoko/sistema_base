DROP TABLE accion_grupo;
DROP TABLE usuario;
DROP TABLE accion;
DROP TABLE grupo;
DROP TABLE seccion;




CREATE TABLE  grupo(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    observaciones TEXT,
    status TINYINT(1) NOT NULL)ENGINE=InnoDB;


CREATE TABLE seccion(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    observaciones TEXT,
    status TINYINT(1) NOT NULL
    )ENGINE=InnoDB;

CREATE TABLE  usuario(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user VARCHAR(500) NOT NULL UNIQUE,
    password VARCHAR(50) NOT NULL UNIQUE,
    email VARCHAR(500) NOT NULL UNIQUE,
    grupo_id INT(11) NOT NULL,
    FOREIGN KEY (grupo_id) REFERENCES grupo(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;

CREATE TABLE  accion(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    seccion_id INT(11) NOT NULL,
    FOREIGN KEY (seccion_id) REFERENCES seccion(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;


CREATE TABLE  accion_grupo(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    accion_id INT(11) NOT NULL,
    grupo_id INT(11) NOT NULL,
    FOREIGN KEY (accion_id) REFERENCES accion(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (grupo_id) REFERENCES grupo(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;

ALTER TABLE accion_grupo ADD UNIQUE INDEX accion_grupo (accion_id, grupo_id);


INSERT INTO grupo (descripcion, observaciones, status) VALUES ('Admins','Admins',1);
INSERT INTO usuario (user,password,email,grupo_id) VALUES('root','moro58','mgamboa@tdesystems.com.mx', 1);